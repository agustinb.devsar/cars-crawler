import scrapy


class PassportBMWSpider(scrapy.Spider):
    name = 'passportbmw.com'
    start_urls = ['https://www.passportbmw.com/searchnew.aspx?pn=100&pt=1']

    def parse(self, response):
        for elem in response.css('div.srpVehicle'):
            item = {}

            # default values
            for key, value in elem.attrib.items():
                if key.startswith('data-'):
                    _, attr = key.split('-')
                    item[attr] = value

            # image
            img_path = elem.css('img.vehicleImg::attr(src)').extract_first()
            item['img'] = response.urljoin(img_path)

            yield item

        for next_page in response.css('ul.pagination li:not(.disabled) > a'):
            yield response.follow(next_page, self.parse)