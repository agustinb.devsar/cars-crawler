# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem


class DuplicatesPipeline(object):
    """
    vin = vehicle identification number
    """

    def __init__(self):
        self.vins_seen = set()

    def process_item(self, item, spider):
        vin = item.get('vin')

        if vin not in self.vins_seen:
            self.vins_seen.add(vin)
            return item

        raise DropItem("Duplicate item found: %s" % item)