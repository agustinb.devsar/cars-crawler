# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CarItem(scrapy.Item):
    trim = scrapy.Field()
    engine = scrapy.Field()
    vehicletype = scrapy.Field()
    intcolor = scrapy.Field()
    name = scrapy.Field()
    vehicleid = scrapy.Field()
    make = scrapy.Field()
    intcolorcode = scrapy.Field()
    vin = scrapy.Field()
    extcolor = scrapy.Field()
    modelcode = scrapy.Field()
    bodystyle = scrapy.Field()
    year = scrapy.Field()
    extcolorcode = scrapy.Field()
    model = scrapy.Field()
    trans = scrapy.Field()
    fueltype = scrapy.Field()
    price = scrapy.Field()
    msrp = scrapy.Field()
    img = scrapy.Field()
